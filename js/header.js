document.write(
"<header class='bs-docs-nav navbar navbar-static-top' id='top'>" +	
"    <div class='navbar-header'>" +
"      <button aria-controls='bs-navbar' aria-expanded='false' class='collapsed navbar-toggle' data-target='#bs-navbar' data-toggle='collapse' type='button'>" +
"      <span class='sr-only'>Toggle navigation</span>" +
"      <span class='icon-bar'></span>" +
"      <span class='icon-bar'></span>" +
"      <span class='icon-bar'></span>" +
"      </button>" +
"      <a href='/' class='navbar-brand'><img class='mini-logo' alt='Study Right, private lessons in London, UK. ' src='img/study-right-min-logo.png'></a>" +
"     </div>" +
"      <nav class='collapse navbar-collapse' id='bs-navbar'>" +
"      <ul class='nav navbar-nav'>" +
"       <li>" +
"        <a href='about.html'>About us</a>" +
"       </li>" +
"       <li>" +
"        <a href='courses.html'>Courses</a>" +
"       </li>" +
"       <li>" +
"        <a href='pricing.html'>Pricing</a>" +
"       </li>" +
"       <li>" +
"        <a href='meet-the-team.html'>Meet the team</a>" +
"       </li>" +
"       <li>" +
"        <a href='contact.html'>Contact</a>" +
"       </li>" +
"      </ul>" +
"     </nav>" +
"</header>" +
"<br />"
);