module.exports = function(grunt) {

  grunt.initConfig({ 
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      my_target: {
        files: {
          'js/animate-home-minified.js': ['js/animate-home.js']
        }
      }
    },
    sass: {
        dist: {
            files: {
                'css/style-sassed.css': ['css/style.scss']
            }
        }
    },
autoprefixer: {
    target: {
      files:{
          'css/style-prefixed.css':['css/style-sassed.css']
        }
    }
  },
    cssmin:{
      options: {
      },target: {
        files:{
          'css/style-min.css':['css/style-prefixed.css']
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.registerTask ('default', ['uglify', 'sass', 'autoprefixer', 'cssmin']);

};